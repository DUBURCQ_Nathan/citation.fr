import { Application } from "express";
import ProjetController from "./controllers/ProjetController";

export default function route(app: Application)
{

    app.get('/backoff', (req, res) =>
    {
        ProjetController.backoff(req, res);
    });

    app.get('/accueil', (req, res) =>
    {
        ProjetController.accueil(req, res);
    });





    app.get('/auteurs', (req, res) =>
    {
        ProjetController.auteurs(req, res)
    });

    app.get('/auteur-create', (req, res) =>
    {
        ProjetController.showForm(req, res)
    });

    app.post('/auteur-create', (req, res) =>
    {
        ProjetController.create(req, res)
    });

    app.get('/auteur-read/:id', (req, res) =>
    {
        ProjetController.read(req, res)
    });

    app.get('/auteur-update/:id', (req, res) =>
    {
        ProjetController.showFormUpdate(req, res)
    });

    app.post('/auteur-update/:id', (req, res) =>
    {
        ProjetController.update(req, res)
    });

    app.get('/auteur-delete/:id', (req, res) =>
    {
        ProjetController.delete(req, res)
    });




    app.get('/citations', (req, res) =>
    {
        ProjetController.citations(req, res)
    });

    app.get('/citation-create', (req, res) =>
    {
        ProjetController.showFormCit(req, res)
    });

    app.post('/citation-create', (req, res) =>
    {
        ProjetController.createCit(req, res)
    });

    app.get('/citation-read/:id', (req, res) =>
    {
        ProjetController.readCit(req, res)
    });

    app.get('/citation-update/:id', (req, res) =>
    {
        ProjetController.showFormUpdateCit(req, res)
    });

    app.post('/citation-update/:id', (req, res) =>
    {
        ProjetController.updateCit(req, res)
    });

    app.get('/citation-delete/:id', (req, res) =>
    {
        ProjetController.deleteCit(req, res)
    });
}
