INSERT INTO auteurs ("prenom", "nom") VALUES ('MARC', 'AURÈLE');
INSERT INTO auteurs ("prenom", "nom") VALUES ('ANTOINE', 'DE SAINT EXUPÉRY');
INSERT INTO auteurs ("prenom", "nom") VALUES ('MARCEL', 'PROUST');
INSERT INTO auteurs ("prenom", "nom") VALUES ('STEFAN', 'ZWEIG');
INSERT INTO auteurs ("prenom", "nom") VALUES ('EDGAR', 'MORIN');
INSERT INTO auteurs ("prenom", "nom") VALUES ('HENRI', 'LABORIT');
INSERT INTO auteurs ("prenom", "nom") VALUES ('CARLOS', 'CASTANEDA');
INSERT INTO auteurs ("prenom", "nom") VALUES ('PIERRE', 'BOTTERO');

INSERT INTO citations ("contenu", "auteurs_id") VALUES ('Puissé-je avoir la sérénité d’accepter les choses que je ne peux changer, le courage de changer les choses qui peuvent l’être et la sagesse d’en connaitre la différence.', '1');
INSERT INTO citations ("contenu", "auteurs_id") VALUES ('Si un âne te donne un coup de pied, ne lui rends pas.', '5');
INSERT INTO citations ("contenu", "auteurs_id") VALUES ('A lécole de la vie, aucune expérience, nest fortuite.', '7');
INSERT INTO citations ("contenu", "auteurs_id") VALUES ('Donnez à ceux que vous aimez des ailes pour voler, des racines pour revenir, et des raisons de rester.', '3');
INSERT INTO citations ("contenu", "auteurs_id") VALUES ('Nul ne peut atteindre laube sans passer par le chemin de la nuit.', '6');
INSERT INTO citations ("contenu", "auteurs_id") VALUES ('Où le pied ne va pas, le regard peut atteindre ; où le regard s arrête, lesprit peut continuer.', '2');
INSERT INTO citations ("contenu", "auteurs_id") VALUES ('Le mal vient à cheval et le bonheur à pied.', '8');