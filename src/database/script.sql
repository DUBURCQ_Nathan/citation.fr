-- Creator:       MySQL Workbench 6.3.8/ExportSQLite Plugin 0.1.0
-- Author:        Nathan DUBURCQ
-- Caption:       New Model
-- Project:       Citation.fr
-- Changed:       2022-02-21 11:01
-- Created:       2022-02-21 11:01
PRAGMA foreign_keys = OFF;

-- Schema: BDD Citation.fr
BEGIN;
CREATE TABLE "auteurs"(
  "id" INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
  "nom" VARCHAR(45) NOT NULL,
  "prenom" VARCHAR(45) NOT NULL
);
CREATE TABLE "citations"(
  "id" INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
  "auteurs_id" INTEGER NOT NULL,
  "contenu" VARCHAR(45) NOT NULL,
  CONSTRAINT "fk_citations_auteurs"
    FOREIGN KEY("auteurs_id")
    REFERENCES "auteurs"("id")
);
CREATE INDEX "citations.fk_citations_auteurs_idx" ON "citations" ("auteurs_id");
COMMIT;
