import { Request, Response } from "express-serve-static-core";

export default class ProjetController
{

    static accueil(req: Request, res: Response): void
    {
        const db = req.app.locals.db;
        const citation = db.prepare('SELECT * FROM citations INNER JOIN auteurs ON auteurs.id = citations.auteurs_id ORDER BY `id` DESC LIMIT 1').all();
        res.render('pages/accueil', {
            title: 'Welcome',
            citation: citation
        });
    }

    static backoff(req: Request, res: Response): void
    {
        const db = req.app.locals.db;
        const auteur = db.prepare('SELECT * FROM auteurs').all();
        const citation = db.prepare('SELECT * FROM citations INNER JOIN auteurs ON auteurs.id = citations.auteurs_id').all();
        res.render('pages/backoff', {
            auteur: auteur ,
            citation: citation
        });
    }


    /**
     * Affiche la liste des auteurs
     * @param req 
     * @param res 
     */
    static auteurs(req: Request, res: Response)
    {
        const db = req.app.locals.db;

        const auteur = db.prepare('SELECT * FROM auteurs').all();

        res.render('pages/Auteurs/auteurs', {
            title: 'Les Auteurs',
            auteur: auteur
        });
    }

    /**
     * Affiche le formulaire de creation d'auteur
     * @param req 
     * @param res 
     */
    static showForm(req: Request, res: Response): void
    {
        res.render('pages/Auteurs/auteur-create');
    }

    /**
     * Recupere le formulaire et insere l'auteur en db
     * @param req 
     * @param res 
     */
    static create(req: Request, res: Response): void
    {
        const db = req.app.locals.db;

        db.prepare('INSERT INTO auteurs ("nom", "prenom") VALUES (?,?)').run(req.body.nom, req.body.prenom);
/*      db.prepare('SELECT client_id FROM client WHERE id = ?').get(req.params.id);

        db.prepare('INSERT INTO projet ("title", "content") VALUES (?, ?)').run(req.body.title, req.body.content);
*/
        ProjetController.auteurs(req, res);
    }

    /**
     * Affiche 1 auteur
     * @param req 
     * @param res 
     */
    static read(req: Request, res: Response): void
    {
        const db = req.app.locals.db;

        const auteur = db.prepare('SELECT * FROM auteurs WHERE id = ?').get(req.params.id);

        res.render('pages/accueil', {
            auteur: auteur
        });
    }

    /**
     * Affiche le formulaire pour modifier un auteur
     * @param req 
     * @param res 
     */
    static showFormUpdate(req: Request, res: Response)
    {
        const db = req.app.locals.db;

        const auteur = db.prepare('SELECT * FROM auteurs WHERE id = ?').get(req.params.id);


        res.render('pages/Auteurs/auteur-update', {
            auteur: auteur
        });
    }

    /**
     * Recupere le formulaire de l'auteur modifié et l'ajoute a la database
     * @param req 
     * @param res 
     */
    static update(req: Request, res: Response)
    {
        const db = req.app.locals.db;

        db.prepare('UPDATE auteurs SET nom = ?, prenom = ? WHERE id = ?').run(req.body.nom, req.body.prenom, req.params.id);

        ProjetController.auteurs(req, res);
    }

    /**
     * Supprime un auteur
     * @param req 
     * @param res 
     */
    static delete(req: Request, res: Response)
    {
        const db = req.app.locals.db;

        db.prepare('DELETE FROM auteurs WHERE id = ?').run(req.params.id);

        ProjetController.auteurs(req, res);
    }

    // SELECT * FROM citations INNER JOIN auteurs WHERE citations.id = auteurs.fk_id
    // SELECT * FROM auteurs INNER JOIN citations WHERE auteurs.id = citations.fk_id
    // SELECT * FROM table1 INNER JOIN table2 WHERE table1.id = table2.fk_id
    // SELECT * FROM citations INNER JOIN auteurs WHERE citations.auteurs_id = auteurs.id
    // SELECT * FROM citations
    // SELECT * FROM auteurs INNER JOIN citations WHERE auteurs.id = citations.fk_citations_auteurs_idx
    // SELECT nom, prenom FROM auteurs INNER JOIN citations ON auteurs.id = citations.auteurs_id
    // SELECT nom, prenom FROM auteurs INNER JOIN citations ON auteurs.id = auteurs.citations_id
    // SELECT nom, prenom FROM auteurs INNER JOIN citations ON auteurs.id = citations.auteurs_id
    
    // SELECT * FROM citations INNER JOIN auteurs ON citations.id = auteurs.citations_id


    // SELECT nom, prenom FROM auteurs INNER JOIN citations ON auteurs.id = citations.auteurs_id

    /**
     * Affiche la liste des citations
     * @param req 
     * @param res 
     */
     static citations(req: Request, res: Response)
     {
         const db = req.app.locals.db;
          const citation = db.prepare('SELECT * FROM citations INNER JOIN auteurs ON auteurs.id = citations.auteurs_id').all();

         res.render('pages/Citations/citations', {
             title: 'Les Citations',
             citation: citation,
         });
     }

    /**
     * Affiche le formulaire de creation d'une citation
     * @param req 
     * @param res 
     */
     static showFormCit(req: Request, res: Response): void
     {
         res.render('pages/Citations/citation-create');
     }
 
     /**
      * Recupere le formulaire et insere la citation en db
      * @param req 
      * @param res 
      */
     static createCit(req: Request, res: Response): void
     {
         const db = req.app.locals.db;
 
         db.prepare('INSERT INTO citations ("contenu", "auteurs_id") VALUES (?,?)').run(req.body.contenu, req.body.auteurs_id);
 /*      db.prepare('SELECT client_id FROM client WHERE id = ?').get(req.params.id);
 
         db.prepare('INSERT INTO projet ("title", "content") VALUES (?, ?)').run(req.body.title, req.body.content);
 */
         ProjetController.citations(req, res);
     }
 
     /**
      * Affiche 1 citation
      * @param req 
      * @param res 
      */
     static readCit(req: Request, res: Response): void
     {
         const db = req.app.locals.db;
 
         const citation = db.prepare('SELECT * FROM citations WHERE id = ?').get(req.params.id);
 
         res.render('pages/accueil', {
             citation: citation
         });
     }
 
     /**
      * Affiche le formulaire pour modifier une citation
      * @param req 
      * @param res 
      */
     static showFormUpdateCit(req: Request, res: Response)
     {
         const db = req.app.locals.db;
 
         const citation = db.prepare('SELECT * FROM citations WHERE id = ?').get(req.params.id);
 
 
         res.render('pages/Citations/citation-update', {
             citation: citation
         });
     }
 
     /**
      * Recupere le formulaire de la citation modifié et l'ajoute a la database
      * @param req 
      * @param res 
      */
     static updateCit(req: Request, res: Response)
     {
         const db = req.app.locals.db;
 
         db.prepare('UPDATE citations SET contenu = ?, auteurs_id = ? WHERE id = ?').run(req.body.contenu, req.body.auteur_id, req.params.id);
 
         ProjetController.citations(req, res);
     }
 
     /**
      * Supprime une citation
      * @param req 
      * @param res 
      */
     static deleteCit(req: Request, res: Response)
     {
         const db = req.app.locals.db;
 
         db.prepare('DELETE FROM citations WHERE id = ?').run(req.params.id);
 
         ProjetController.citations(req, res);
     }
 }